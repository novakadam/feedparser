"""
Aggreagator module which can collect rss and atom links from html files and dump
them into a JSON file, grouped by their type

module author: Adam Novak <adano@hotmail.hu>
"""

__all__ = ['FeedParser']

from HTMLParser import HTMLParser
import json
import logging

log = logging.getLogger('feedparser.parser')

class FeedParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.links = dict()
        self.links['rss'] = []
        self.links['atom'] = []

    def handle_starttag(self, tag, attrs):
        if tag == 'a' or tag == 'link':
            attributes = dict()
            for attr in attrs:
                attributes[attr[0]] = attr[1]
            if 'href' in attributes and 'type' in attributes:
                if attributes['type'] == 'application/rss+xml':
                    self.links['rss'].append(attributes['href'])
                elif attributes['type'] == 'application/atom+xml':
                    self.links['atom'].append(attributes['href'])

    def load_html(self, path):
        file = open(path, 'r')
        retval = file.read()
        log.info('[PARSER] Successfully loaded HTML file')
        return retval
    
    def get_rss_links(self):
        return self.links['rss']

    def get_atom_links(self):
        return self.links['atom']

    def dump_json(self, path):
        with open(path, 'w') as fp:
            json.dump(self.links, fp, sort_keys=True, indent=4, separators=(',', ': '))
        log.info('[PARSER] Successfully dumped links into JSON file')

    def get_feeds_from_html(self, html_path, output):
        data = self.load_html(html_path)
        self.feed(data)
        log.info('[PARSER] Parsing completed')
        self.dump_json(output)
