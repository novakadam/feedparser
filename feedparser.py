from parser import FeedParser
import logging
import logging.config
import argparse


logging.config.fileConfig('logging.conf')

log = logging.getLogger('feedparser')

def collect_feeds(html_path='input.html', output='output.json'):
    log.info('[MAIN] Parsing feed URLs from input file %r and saving them as %r', html_path, output)
    parser = FeedParser()
    parser.get_feeds_from_html(html_path, output)
    log.info('[MAIN] Done')

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description='RSS and Atom feed collector from HTML files.')
    argparser.add_argument('-i', '--input', default='input.html', help='Path of the input HTML file', dest='html_path')
    argparser.add_argument('-o', '--output', default='output.json', help='Path of the output JSON file', dest='json_path')
    args = argparser.parse_args()
    collect_feeds(args.html_path, args.json_path)
