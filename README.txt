    __  __________  _____       ______              __   ____                           
   / / / /_  __/  |/  / /      / ____/__  ___  ____/ /  / __ \____ ______________  _____
  / /_/ / / / / /|_/ / /      / /_  / _ \/ _ \/ __  /  / /_/ / __ `/ ___/ ___/ _ \/ ___/
 / __  / / / / /  / / /___   / __/ /  __/  __/ /_/ /  / ____/ /_/ / /  (__  )  __/ /    
/_/ /_/ /_/ /_/  /_/_____/  /_/    \___/\___/\__,_/  /_/    \__,_/_/  /____/\___/_/     
                                                                                        

HTML Feed Parser is a Python package which can collect links of RSS
(type='application/rss+xml') and Atom (type='application/atom+xml')
feeds from a given HTML document(more precisely from the HTML's <a> and <link>
tags).

USAGE: You can run feedparser.py as a python script, or import the Parser module
(and the FeedParser class within it) into your code

- USAGE AS A SCRIPT:
    $  python feedparser.py -h
    usage: feedparser.py [-h] [-i HTML_PATH] [-o JSON_PATH]

    RSS and Atom feed collector from HTML files.

    optional arguments:
    -h, --help            show this help message and exit
    -i HTML_PATH, --input HTML_PATH
                            Path of the input HTML file
    -o JSON_PATH, --output JSON_PATH
                            Path of the output JSON file


Currently, the parser module stores the links in a nonpersistent dictionary,
separated by their type. However, the links of the feeds could be easily stored
in a persistent key-value datastore - e.g. Redis - if needed.
Logging is configured in the 'logging.conf' file

